# CodeSkulptor runs Python programs in your browser.
# Click the upper left button to run this simple demo.

# CodeSkulptor runs in Chrome 18+, Firefox 11+, and Safari 6+.
# Some features may work in other browsers, but do not expect
# full functionality.  It does NOT run in Internet Explorer.

import simplegui
import random
import time

message = "Welcome!"
interval = 1000

# Handler for timer 
def tick():
    global message
    
    
    message = str( (time.time() / 60 / 60 / 24 / 365))

# Handler to draw on canvas
def draw(canvas):
    canvas.draw_text(message, [10,112], 30, "Red")

# Handler for text box
def input_handler(input):
    global interval
    global timer
    
    interval = int(input)
    timer.stop()
    timer = simplegui.create_timer(interval,tick)
    timer.start()

    
# Create a frame and assign callbacks to event handlers
frame = simplegui.create_frame("Home", 300, 200)
timer = simplegui.create_timer(interval,tick)
                               
txtInterval = frame.add_input('Interval', input_handler, 50)
txtInterval.set_text(str(interval))

frame.set_draw_handler(draw)

# Start the frame animation
frame.start()
timer.start()
